package ru.ntsu.nikitoser;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    ProductController(ProductRepository repository)
    {
        this.productRepository = repository;
    }


    // Aggregate root
    // tag::get-aggregate-root[]
    @GetMapping("/products")
    List<Product> all() {
        return productRepository.findAll();
    }
    // end::get-aggregate-root[]


    @PostMapping("/products")
    public Product newProduct(@RequestBody Product newProduct)
    {
        return productRepository.save(newProduct);
    }

    @GetMapping("/products/{id}")
    public Product one(@PathVariable Long id)
    {
        return productRepository.findById(id)
                .orElseThrow( () -> new ProductNotFoundException(id));
    }

    @PutMapping("/products/{id}")
    public Product replaceProduct(@RequestBody Product newProduct, Long id)
    {
        return productRepository.findById(id)
                .map(product -> {
                    product.setName(newProduct.getName());
                    product.setCountry(newProduct.getCountry());
                    product.setPrice(newProduct.getPrice());
                    product.setProducer(newProduct.getProducer());
                    return productRepository.save(product);
                })
                .orElseGet(()-> {
                    newProduct.setId(id);
                    return productRepository.save(newProduct);
                });

    }

    @DeleteMapping("product/{id}")
    public void deleteProduct(@PathVariable Long id)
    {
        productRepository.deleteById(id);
    }
//
//    @RequestMapping(value = "/products", method = RequestMethod.GET)
//    public Page<Product> listProduct(
//            @RequestParam(name = "page", defaultValue = "0") int page,
//            @RequestParam(name = "size", defaultValue = "5") int size){
//        return productRepository.findAll(PageRequest.of(page, size));
//    }
//
//    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET)
//    public Optional<Product> getProduct(@PathVariable("id") Long id) {
//        return productRepository.findById(id);
//
//    }
//
//    @RequestMapping(value = "/prod", method = RequestMethod.POST)
//    public Product addProduct ( @RequestBody Product p) {
//        return productRepository.saveAndFlush(p);
//    }
//
//    @RequestMapping(value = "/prod/{id}", method = RequestMethod.PUT)
//    public Product updatePoduct (
//            @RequestBody Product p,
//            @PathVariable("id") Long id) {
//        p.setId(id);
//        return productRepository.saveAndFlush(p);
//
//    }
//
//    @RequestMapping(value = "/prod/{id}", method = RequestMethod.DELETE)
//    public void deleteProduct(@PathVariable("id") Long id) {
//        productRepository.deleteById(id);
//    }

}
