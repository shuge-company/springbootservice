package ru.ntsu.nikitoser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initDatabase(ProductRepository productRepository)
    {
        return args -> {
            log.info("Preloading " + productRepository.save(new Product("Микроволновка","ЛогПродакшн", 1000,"Россия")));
            log.info("Preloading " + productRepository.save(new Product("Микроволновка","ЩугПродакшн", 1000,"Россия")));
        };
    }
}
