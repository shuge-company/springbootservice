package ru.ntsu.nikitoser;

public class ProductNotFoundException extends RuntimeException{
    ProductNotFoundException(Long id){
        super("Couldnt find product "+ id);
    }
}
